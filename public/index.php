<?php

use Precode\Application;
use Zend\Diactoros\Response;
use Precode\ServiceContainer;
use Precode\Plugins\ViewPlugin;
use Precode\Plugins\RoutePlugin;
use Psr\Http\Message\RequestInterface;
use Precode\Plugins\DbPlugin;

require_once __DIR__ . '/../vendor/autoload.php';

$serviceContainer = new ServiceContainer();

$app = new Application($serviceContainer);

$app->plugin(new RoutePlugin());
$app->plugin(new ViewPlugin());
$app->plugin(new DbPlugin());

/* Registrando os Controllers */

require_once __DIR__ . '/../src/Controllers/CategoryController.php';

$app->run();
<?php

namespace Precode\Plugins;

use Aura\Router\RouterContainer;
use Precode\ServiceContainerInterface;
use Psr\Http\Message\RequestInterface;
use Zend\Diactoros\ServerRequestFactory;
use Interop\Container\ContainerInterface;

class RoutePlugin implements PluginInterface 
{


    public function register(ServiceContainerInterface $container)
    {
        $routerContainer = new RouterContainer();

        $map = $routerContainer->getMap();
        $matcher = $routerContainer->getMatcher();

        $generator = $routerContainer->getGenerator();

        $container->add('routes', $map);
        $container->add('routes.matcher', $matcher);
        $container->add('routes.generator',$generator);

        $request = $this->getRequest();

        $container->add(RequestInterface::class, $request);

        $container->addLazy('route', function(ContainerInterface $container) {
           
            $matcher = $container->get('routes.matcher');
            $request = $container->get(RequestInterface::class);

            return $matcher->match($request);
        });

    }

    protected function getRequest()
    {
        return ServerRequestFactory::fromGlobals(
            $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
        );
    }

}
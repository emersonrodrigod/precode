<?php

namespace Precode\Plugins;

use Precode\ServiceContainerInterface;

interface PluginInterface 
{
    public function register(ServiceContainerInterface $container);
}


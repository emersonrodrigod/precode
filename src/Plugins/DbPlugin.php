<?php

namespace Precode\Plugins;

use Precode\ServiceContainerInterface;

class DbPlugin implements PluginInterface
{

    public function register(ServiceContainerInterface $container)
    {

        $config = include __DIR__ . '/../../config/db.php';
        
        list(
            'driver' => $driver,
            'host' => $host,
            'username' => $username,
            'password' => $password,
            'database' => $database
        ) = $config['default'];

        $dsn = "$driver:host={$host};dbname={$database}";

        $connection =  new \PDO($dsn, $username, $password);

        $container->add('DB', $connection);
        
    }

}
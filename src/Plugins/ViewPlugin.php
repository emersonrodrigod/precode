<?php

namespace Precode\Plugins;

use Precode\ServiceContainerInterface;
use Psr\Container\ContainerInterface;
use Precode\View\ViewRenderer;

class ViewPlugin implements PluginInterface 
{


    public function register(ServiceContainerInterface $container)
    {

        $container->addLazy('twig', function(ContainerInterface $container){
            
            $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../views');
            $twig = new \Twig_Environment($loader);

            return $twig;
        });

        $container->addLazy('view.renderer',function(ContainerInterface $container){
            $twigEnvironment = $container->get('twig');
            return new ViewRenderer($twigEnvironment);
        });
    }

}
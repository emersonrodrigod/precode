<?php

namespace Precode;

use Precode\Plugins\PluginInterface;
use Precode\ServiceContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\SapiEmitter;
use Zend\Diactoros\Response\RedirectResponse;

class Application {

    private $serviceContainer;

    public function __construct(ServiceContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    public function service($name)
    {
        return $this->serviceContainer->get($name);
    }

    public function addService(string $name, $service)
    {
        if(is_callable($service)){
            $this->serviceContainer->addLazy($name, $service);
        }else{
            $this->serviceContainer->add($name, $service);
        }
    }

    public function plugin(PluginInterface $plugin)
    {
        $plugin->register($this->serviceContainer);
    }

    public function get($path, $action, $name = null)
    {
        $router = $this->service('routes');
        $router->get($name, $path, $action);
        return $this;
    }

    public function post($path, $action, $name = null)
    {
        $router = $this->service('routes');
        $router->post($name, $path, $action);
        return $this;
    }

    public function redirect($path)
    {
        return new RedirectResponse($path);
    }

    protected function response(ResponseInterface $response)
    {
        $emitter = new SapiEmitter();
        $emitter->emit($response);
    }

    public function run()
    {
        $route = $this->service('route');

        $request = $this->service(RequestInterface::class);

        if(!$route){
            echo "Page not found!";
            exit;
        }

        foreach($route->attributes as $key => $value)
        {   
            $request = $request->withAttribute($key, $value);
        }

        $callable = $route->handler;

        $response = $callable($request);
        $this->response($response);
    }

}
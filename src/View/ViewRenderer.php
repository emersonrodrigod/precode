<?php

namespace Precode\View;

use Zend\Diactoros\Response;

class ViewRenderer implements ViewRendererInterface
{

    private $twigEnvironment;

    public function __construct(\Twig_Environment $twigEnvironment)
    {
        $this->twigEnvironment = $twigEnvironment;
        
    }

    public function render(string $template, array $context = [])
    {
        $environment = $this->twigEnvironment->render($template, $context);
        $response = new Response();
        $response->getBody()->write($environment);
    
        return $response;
        
    }

}
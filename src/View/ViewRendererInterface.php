<?php

namespace Precode\View;

interface ViewRendererInterface
{

    public function render(string $template, array $context = []);

}
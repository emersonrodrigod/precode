<?php

use Psr\Http\Message\RequestInterface;
use Precode\Models\Category;

/* Rota Principal da aplicação */
$app->get('/', function() use($app){

    return $app->redirect('/categories');

});

$app->get('/categories', function() use($serviceContainer, $app ){

    $category = new Category($serviceContainer);
    $categories = $category->getAll();

    $view = $app->service('view.renderer');
    return $view->render('category/index.html.twig',compact('categories'));
});

$app->get('/category/create', function() use($app) {

    $view = $app->service('view.renderer');
    return $view->render('category/create.html.twig');

});

$app->post('/categories/{id}/update', function(RequestInterface $request) use ($app, $serviceContainer){

    $category = new Category($serviceContainer);
    $data = $request->getParsedBody();

    $category->update($data, $request->getAttribute('id'));

    return $app->redirect('/categories');

});

$app->get('/categories/{id}/edit', function(RequestInterface $request) use ($app, $serviceContainer){

    $category = new Category($serviceContainer);
    $data = $category->find($request->getAttribute('id'));

    $view = $app->service('view.renderer');
    return $view->render('category/edit.html.twig',['category' => $data]);

});

$app->get('/categories/{id}/remove', function(RequestInterface $request) use ($app, $serviceContainer){

    $category = new Category($serviceContainer);
    $data = $category->delete($request->getAttribute('id'));

    if($data)
    {
        return $app->redirect('/categories');
    }
 
});

$app->post('/categories/store', function(RequestInterface $request) use($serviceContainer, $app ){

    $category = new Category($serviceContainer);
    $data = $request->getParsedBody();

    $category->create($data);
    return $app->redirect('/categories');
});


$app->get('/teste/{name}', function(RequestInterface $request) use($app){
    
    $name = $request->getAttribute('name');
    
    $view = $app->service('view.renderer');
    return $view->render('category/index.html.twig',compact('name'));
});



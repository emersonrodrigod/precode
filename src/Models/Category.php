<?php

namespace Precode\Models;

class Category extends Model
{

    public function getAll()
    {

        $stmt = $this->connection->query("select * from categories");
        return $stmt->fetchAll();
    }

    public function create($data)
    {
        $stmt = $this->connection->prepare('INSERT INTO categories VALUES(null,:name)');
        
        return $stmt->execute([
            ':name' => $data['name']
        ]);
    }

    public function update($data, $id)
    {
          
        $stmt = $this->connection->prepare('UPDATE categories SET name = :name WHERE id = :id');
        
        return $stmt->execute(array(
            ':id'   => $id,
            ':name' => $data['name']
        ));

    }

    public function delete($id)
    {
          
        $stmt = $this->connection->prepare('DELETE FROM categories WHERE id = :id');
        
        return $stmt->execute(array(
            ':id'   => $id
        ));

    }

    public function find($id)
    {
        $stmt = $this->connection->prepare('SELECT * FROM categories WHERE id = :id');
        
        $stmt->execute(array(
            ':id'   => $id,
        ));

        return $stmt->fetch();

    }

}
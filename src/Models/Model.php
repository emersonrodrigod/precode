<?php

namespace Precode\Models;

use Precode\ServiceContainerInterface;

class Model
{

    protected $connection;
   
    public function __construct(ServiceContainerInterface $container)
    {
        $this->connection = $container->get('DB');
    }

}